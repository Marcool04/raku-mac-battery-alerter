#!/usr/bin/env raku

use Mac::Battery::Alerter::i18n;

unit sub MAIN(
  #| (Int) Battery percentage under which to warn.
  #| Must be > panic, and < panic+10 (default: 5%)
  Int :w(:warn($OPT_warn_percentage)) = 5,
  #| (Int) Battery percentage under which to panic (default: 4%)
  Int :p(:panic($OPT_panic_percentage)) = 4,
  #| (Str) Two letter language code to force a language (default: $OPT_language, or "en"),
  Str :l(:lang(:language($OPT_language))) = (
    (%*ENV<LANG>.defined && (%*ENV<LANG> ∉ <en fr>)) ?? %*ENV<LANG>.substr(0..1) !! "en"
  ),
  #| (Bool) Print debug messages (default: False)
  Bool :d(:debug($OPT_debug)) = False,
  #| (Str) A file to store state, used to remind script if it has shouted,
  #| and then say thanks (default: "/tmp/battery_script_state_file")
  Str :state_file($OPT_state_file)= "/tmp/battery_script_state_file",
  #| (Int) The volume to set audio level to before speeking (default: 80%)
  Int :s(:sound($OPT_sound_volume)) = 80,
);

debug("Command line arguments: " ~ @*ARGS.gist);

# Language setup
debug("Will use language code: $OPT_language");
my $i18n = Mac::Battery::Alerter::i18n.new(language => $OPT_language);

# Check warn/panic settings
if ($OPT_warn_percentage < $OPT_panic_percentage) {
  say "Invalid combination of warn and panic settings.";
  say "Warn: $OPT_warn_percentage can't be smaller than panic: $OPT_panic_percentage.";
  say $*USAGE;
  exit;
}
if ($OPT_warn_percentage > $OPT_panic_percentage+10) {
  say "Invalid combination of warn and panic settings.";
  say "Warn: $OPT_warn_percentage can't be greater than panic+10: "~ ($OPT_panic_percentage+10);
  say $*USAGE;
  exit;
}

# Start
debug("Starting $*PROGRAM-NAME at " ~ DateTime.now());
my (Bool $on_battery, Int $percentage) = get_battery_status();
if (! $on_battery) {
  debug("Currently plugged in (not on battery)");
  if (have_shouted()) {
    debug("Already shouted, saying thanks.");
    speek( $i18n.tl('thanks') );
    debug("Now forgetting we ever shouted.");
    have_shouted(False);
    debug("All done.");
  } else {
    debug("Have not shouted, so nothing to do. Cherrio!");
  }
  debug("Plugged in action complete. Exiting.");
  exit;
}
debug("Currently not plugged in (on battery)");
if ($percentage ≤ $OPT_warn_percentage) {
  debug("Percentage $percentage is below $OPT_warn_percentage. Warning.");
  speek($i18n.tl('low-battery'));
	if ($percentage ≤ $OPT_panic_percentage) {
    debug("And even lower than $OPT_panic_percentage. Panic warning.");
		speek($i18n.tl('panic'));
	}
  have_shouted(True);
} else {
  debug("Percentage $percentage is above $OPT_warn_percentage. Not doing anything.");
}
debug("Not plugged in action complete. Exiting.");
exit;


#############
# FUNCTIONS #
#############
sub debug(
  Str $message
) {
  say $message if ($OPT_debug == True);
}

sub get_battery_status() {
  debug("Getting battery status.");
  my Bool $on_battery;
  my Str $batt_status_line = run("pmset", "-g", "batt", :out).out.lines[1];
  debug("Battery status line: $batt_status_line");
  # looks like :
  # -InternalBattery-0 (id=11468899)	74%; discharging; 5:39 remaining present: true
  # -InternalBattery-0 (id=11468899)	73%; AC attached; not charging present: true
  # -InternalBattery-0 (id=11468899)	73%; charging; (no estimate) present: true

  my Regex $batt_status_regex = /\-InternalBattery\-0\s\(id\=\d+\)\s+ $<percentage>=[\d ** 1..3]\%\;\s$<status>=[.*]/;
  if (! $batt_status_line.match($batt_status_regex) ) {
    die "Failed to match line $batt_status_line to regex "~$batt_status_regex.gist;
  }
  my Str $status = ~$<status>.Str;
  if (
    $status.contains("discharging")
  ) {
    $on_battery = True;
  } elsif (
    $status.contains("AC attached") ||
    $status.contains("charging") ||
    $status.contains("charged")
  ) {
    $on_battery = False;
  } else {
    die "Invalid charge status returned from matching \"pmset -g batt\": $status";
  }
  debug("On battery is: " ~ $on_battery);
  my Str $percentage_string = $<percentage>.Str;
  debug("Percentage string: $percentage_string");
  my Int $percentage;
  try {
    $percentage = $percentage_string.Int;
    CATCH {
      when X::Str::Numeric {
        die "$percentage couldn't be converted to an integer";
      }
    }
  }
  if (! $percentage ∈ (1..100)) {
    die "$percentage is not in the range 1 to 100.";
  }
  return ($on_battery, $percentage);
}

multi volume(
  Int $volume_level,
) {
  if (
    run("system_profiler", "SPAudioDataType", :out).out.lines.grep(/Headphones/).so
  ) {
    debug("On headphones: not adjusting volume.");
    return;
  }
  debug("Setting volume level to $volume_level");
  run("osascript", "-e", "set volume output volume $volume_level");
  volume(); # just to print debug lines if needed from a volume status
}

multi volume(
  --> Int
) {
  debug("Getting current volume level.");
  my Int $volume = run(
      "osascript", "-e", 'output volume of (get volume settings)', :out
    ).out.slurp.Int;
  if ( ! ($volume ∈ 0..100 ) ) {
    die "Volume $volume from \"osascript output volume of get volume settings\""~
      " not a number between 0 and 100"
  }
  debug("Volume level is: $volume");
  return $volume;
}

sub speek (
  Str $this
) {
  my Int $old_volume_level = volume();
  volume($OPT_sound_volume);
  debug("Speeking: $this");
  run("say", "-v", $i18n.tl('apple-say-voice'), "\"$this\"");
  volume($old_volume_level);
}

multi have_shouted(
  Bool $set_to_this
) {
  if ($set_to_this eq $OPT_state_file.IO.e) {
    debug("Asked to change shouted state but file condition is suitable. Nothing to do.");
    return;
  }
  if ($set_to_this) {
    debug("Creating shouted state file.");
    $OPT_state_file.IO.open(:w).spurt("");
  } else {
    debug("Deleting shouted state file.");
    $OPT_state_file.IO.unlink;
  }
  return;
}

multi have_shouted(
  --> Bool
) {
  my Bool $result = $OPT_state_file.IO.e;
  debug("Have shouted is: $result");
  return $result;
}
