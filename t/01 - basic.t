#!/usr/bin/env raku

use Test;

my $folder = $*PROGRAM.dirname;
my $executable = "$folder/../bin/battery_alerter.raku";

is run($executable, "-d").exitcode, 0, "executable seems to run ok (code 0)";

done-testing;

exit;
