# GETTING STARTED

To install the script run:

````
$ zef install Mac::Battery::Alerter
````

Then test it to see it is functioning properly:

````
$ /raku/install/path/bin/battery_alerter.raku --debug
````

Now, to run the script on a regular basis and get spoken to when the mac's battery is low, you can use a plist file similar to this one:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>KeepAlive</key>
	<false/>
	<key>Label</key>
	<string>batteryAlert</string>
	<key>LowPriorityIO</key>
	<true/>
  <key>ProgramArguments</key>
  <array>
    <string>/usr/local/bin/battery_script.raku</string>
    <!-- <string>--debug</string>
    <string>--warn=60</string>
    <string>--panic=58</string> -->
  </array>
  <!-- <key>StandardOutPath</key>
  <string>/tmp/battery_script.txt</string>
  <key>StandardErrorPath</key>
  <string>/tmp/battery_script.txt</string>-->
	<key>RunAtLoad</key>
	<true/>
	<key>StartInterval</key>
	<integer>30</integer>
	<key>EnvironmentVariables</key>
  <dict>
      <key>PATH</key>
      <string>/usr/local/bin:/usr/local/sbin:/usr/bin:/bin:/usr/sbin:/sbin</string>
  </dict>
</dict>
</plist>
```

This should be placed in `~/Library/LaunchAgents/` (for instance) and started using:

````
launchctl enable batteryAlert
launchctl start batteryAlert
````

See https://www.launchd.info/ for more information

**Note:** there are commented out entries in this plist which allow you to debug the service should it not appear to be working. Simply uncomment those parts, reload the service, and check the /tmp/battery_script.txt file for errors.


# DESCRIPTION

This is a small script that interfaces with macOS system utilities to analyse what the current battery status is, and uses speech (macOS `say` utility) to warn when the battery is getting low. This is in response to the fact that there is no audible feedback when the "Low battery" dialog is shown, which is rather late (4% battery) and will give you little time to intervene and plug the mac in should you be looking away at that moment.
It is inspired by this post: https://hints.macworld.com/article.php?story=20110219013713667, credit goes to Mac OSX Hints user `Toggi3` for formulating the problem so well and outlining this solution.


# AUTHOR
Mark Collins (https://gitlab.com/Marcool04)


# COPYRIGHT AND LICENCE
Copyright 2020 Mark Collins

This work is free. You can redistribute it and/or modify it under the terms of the Do What The Fuck You Want To Public License, Version 2, as published by Sam Hocevar. See the LICENCE file for (almost no) more details.
