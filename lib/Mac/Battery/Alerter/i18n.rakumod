use I18n::Simple;

unit class Mac::Battery::Alerter::i18n;

has Str $.language where {.chars == 2};

submethod BUILD(:$language) {
  $!language  :=  $language;
  my $lang_file = "i18n/"~$language~".yml";
  i18n-init(%?RESOURCES{$lang_file});
}

method tl (
  Str $string,
  --> Str
) {
  return i18n($string);
}
